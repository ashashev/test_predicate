/*
 * classes.cpp
 *
 *  Created on: 23.11.2016
 *      Author: alesh2
 */
#include "classes.h"

Settings::Settings(const std::string& name):
    name_(name)
{}

Settings::Settings(const char* name):
    name_(name)
{}

unsigned long long Object::counter = 0;

Object::Object(const Settings& settings):
    settings_(settings),
    number_(counter++)
{
}

const Settings& Object::getSettings() const
{
    return settings_;
}

const unsigned long long Object::getNumber() const
{
    return number_;
}

