/*
 * classes.h
 *
 *  Created on: 23.11.2016
 *      Author: alesh2
 */

#ifndef CLASSES_H_
#define CLASSES_H_

#include <string>

class Interface
{
public:
    virtual const unsigned long long getNumber() const = 0;

    virtual ~Interface() {}
};

struct Settings
{
    Settings(const std::string& name);
    Settings(const char* name);

    std::string name_;
};

class Object: public Interface
{
public:
    explicit Object(const Settings& settings);

    const Settings& getSettings() const;
    const unsigned long long getNumber() const;

    static unsigned long long counter;

private:
    const Settings settings_;
    const unsigned long long number_;
};

#endif /* CLASSES_H_ */
