/*
 * cancall.h
 *
 *  Created on: 23.11.2016
 *      Author: alesh2
 */

#ifndef CANCALL_H_
#define CANCALL_H_

template<bool, typename T>
struct enable_if;

template<typename T>
struct enable_if<true, T>
{
    typedef T type;
};


template<typename U> U get();

template<typename T, typename Ret_, typename Arg_>
class CanCall
{
    struct Checker: public T
    {
        Ret_ f[2];
        using T::operator();
        Checker operator()(...) const;
    };

    typedef char yes;
    typedef int no;
    static yes test(Ret_);
    static no test(...);
public:
    static const bool value = sizeof(test(get<Checker>()(get<Arg_>()))) == sizeof(yes);
};

template<typename R_, typename A_, typename Ret_, typename Arg_>
class CanCall<R_ (*) (A_), Ret_, Arg_>
{
    typedef char yes;
    typedef int no;
    struct fail
    {
        Ret_ f[2];
    };
    static R_ call(A_);
    static fail call(...);

    static yes test(Ret_);
    static no test(...);
public:
    static const bool value = sizeof(test(call(get<Arg_>()))) == sizeof(yes);
};

#endif /* CANCALL_H_ */
