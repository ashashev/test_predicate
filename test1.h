/*
 * test1.h
 *
 *  Created on: 23.11.2016
 *      Author: alesh2
 */

#ifndef TEST1_H_
#define TEST1_H_

template <typename T, typename Ret_, typename Arg_>
struct HasParentheses
{
    struct Extra: public T
    {
        Ret_ f1[2];
        using T::operator();
        Extra operator()(...) const;
    };

    template<typename R> static R stub();
    template<typename U, Ret_ (U::*) (Arg_) const> struct SFINAE {};
    template<typename U> static char Test(SFINAE<U, &U::operator()>*);
    template<typename U> static int Test(...);

    static const bool has = sizeof(Test<T>(0)) == sizeof(char);
    static const bool can = sizeof(stub<Extra>()(stub<Arg_>())) == sizeof(Ret_);
};


#endif /* TEST1_H_ */
