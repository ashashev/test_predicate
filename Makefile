app=test_predicate

objs = main.o classes.o ContA.o

CXXFLAGS += -Wall

$(app): $(objs)
	$(CXX) -o $(app) $(objs)

all: $(app)

clean:
	rm -rf -- $(app) $(objs)

main.o: classes.h test.h test1.h cancall.h ContA.h

classes.o: classes.h

ContA.o: ContA.h cancall.h

.PHONY: clean

