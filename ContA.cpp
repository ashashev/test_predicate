/*
 * ContA.cpp
 *
 *  Created on: 23.11.2016
 *      Author: hokum
 */

#include "ContA.h"


ContA::~ContA()
{
    while(!objects_.empty())
    {
        Object* p = objects_.back();
        if (p)
            delete p;
        objects_.pop_back();
    }
}

Object* ContA::add(const Settings& settings)
{
    objects_.push_back(new Object(settings));
    return objects_.back();
}
