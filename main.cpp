/*
 * main.cpp
 *
 *  Created on: 22.11.2016
 *      Author: hokum
 */
#include "classes.h"
#include "test.h"
#include "test1.h"
#include "cancall.h"
#include "ContA.h"

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

struct Foo
{
    explicit Foo(unsigned long long value):
        value_(value)
    {}

    const unsigned long long value_;

#if 1
    int operator()(const Interface* obj) const
    {
        return obj->getNumber() == value_;
    }
#endif

#if 0
    bool operator()(const Settings&) const
    {
        return true;
    }
#endif
};

bool find_obj2(const Settings& settings)
{
    static const std::string t = "obj2";
    return settings.name_ == t;
}

#define FIND(cont, pred) \
{\
    Object* o = (cont).find((pred));\
    cout << "Predicate: "<< #pred << ": " << (o? o->getSettings().name_: "not found") << endl;\
}\

int main()
{
#if 1
    typedef bool (*foo1_ptr) (const Interface *);
    typedef int (*foo2_ptr) (const Interface *);
    typedef int (*foo3_ptr) (Interface *);

    cout << "---------" << endl;
    cout << boolalpha << CanCall<Foo, bool, const Object*>::value << endl;
    cout << boolalpha << CanCall<foo1_ptr, bool, const Object*>::value << endl;
    cout << boolalpha << CanCall<foo2_ptr, bool, const Object*>::value << endl;
    cout << boolalpha << CanCall<foo3_ptr, bool, const Object*>::value << endl;
#endif

    cout << "---------" << endl;
    ContA contA;
    contA.add("obj1");
    contA.add("obj2");
    contA.add("obj3");

    FIND(contA, Foo(2));
    FIND(contA, Foo(1));
    FIND(contA, &find_obj2);

    return 0;
}

