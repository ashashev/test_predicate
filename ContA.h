/*
 * ContA.h
 *
 *  Created on: 23.11.2016
 *      Author: hokum
 */

#ifndef CONTA_H_
#define CONTA_H_

#include "classes.h"
#include "cancall.h"

#include <vector>

class ContA
{
    typedef std::vector<Object*> Objects;
public:
    ~ContA();
    Object* add(const Settings& settings);

    template<typename Pred_>
    typename enable_if< CanCall<Pred_, bool, const Settings&>::value, Object*>::type
    find(Pred_ pred)
    {
        for(Objects::const_iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            if ((*it != NULL) && pred((*it)->getSettings()))
                return *it;
        }
        return NULL;
    }

    template<typename Pred_>
    typename enable_if< CanCall<Pred_, bool, const Object*>::value, Object*>::type
    find(Pred_ pred)
    {
        for(Objects::const_iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            if ((*it != NULL) && pred(*it))
                return *it;
        }
        return NULL;
    }
private:
    Objects objects_;
};

#endif /* CONTA_H_ */
