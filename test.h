/*
 * test.h
 *
 *  Created on: 23.11.2016
 *      Author: alesh2
 */

#ifndef TEST_H_
#define TEST_H_

#include <string>

namespace types
{
enum value {is_function, is_class, is_method};
std::string toString(value v)
{
    switch(v)
    {
    case is_function: return "function";
    case is_class: return "class";
    case is_method: return "method";
    }
    return "unknown";
}
}

template<typename Pred_>
struct Test
{
    static const types::value value = types::is_class;
};

template<typename Pred_, typename Ret_, typename Arg_>
struct Test< Ret_ (Pred_::*)(Arg_) >
{
    static const types::value value = types::is_method;
};

template<typename Pred_, typename Ret_>
struct Test< Ret_ (Pred_::*)(void) >
{
    static const types::value value = types::is_method;
};

template<typename Ret_, typename Arg_>
struct Test< Ret_ (*)(Arg_) >
{
    static const types::value value = types::is_function;
};

template<typename Ret_>
struct Test< Ret_ (*)(void) >
{
    static const types::value value = types::is_function;
};


#endif /* TEST_H_ */
